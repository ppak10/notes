/**
 * index.tsx
 * Home page component
 */

// Node modules
import React from 'react';

// Components
import Navbar from '../components/Navbar';

export default function Home() {
  return (
    <>
      <Navbar />
      <main>
        Home page
      </main>
    </>
  );
}
