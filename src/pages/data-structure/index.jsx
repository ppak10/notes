/**
 * index.jsx
 * Entry file component for displaying data structure notes.
 */

// Node Modules
import React from 'react';

// Components
import Navbar from '../../components/Navbar';

export default function DataStructure() {
  return (
    <>
      <Navbar />
      <main>
        Data Structure notes
      </main>
    </>
  );
}
