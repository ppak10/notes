/**
 * Navbar.tsx
 * Navbar component for routing pages.
 */

// Node Modules
import {Link} from 'gatsby';
import React from 'react';

// Styles
import * as styles from './Navbar.module.css';

export default function Navbar() {
  return (
    <nav className={styles.navbar}>
      <Link to="/">Home</Link>
      <Link to="/data-structure">Data Structure</Link>
    </nav>
  );
}
